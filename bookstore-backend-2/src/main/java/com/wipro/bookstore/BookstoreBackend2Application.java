package com.wipro.bookstore;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.wipro.bookstore.entity.Book;
import com.wipro.bookstore.entity.Completedlist;
import com.wipro.bookstore.entity.User;
import com.wipro.bookstore.repository.BookRepository;
import com.wipro.bookstore.repository.CompletedlistRepository;
import com.wipro.bookstore.repository.UserRepository;
import com.wipro.bookstore.repository.WishlistRepository;
import com.wipro.bookstore.service.UserService;

@SpringBootApplication
public class BookstoreBackend2Application implements CommandLineRunner{

	@Autowired
	private BookRepository bookRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private WishlistRepository wishRepository;
	
	@Autowired
	private CompletedlistRepository completedRepository;
	
	@Autowired
	private UserService userService;
	
	public static void main(String[] args) {
		SpringApplication.run(BookstoreBackend2Application.class, args);
	}
	
	@Override
	public void run(String... args) throws Exception{
		
		
				
		Book b1 = new Book("Ulysses", "James Joyce", "Ulysses by James Joyce", "Fiction", 890, 17);
		Book b2 = new Book("Don Quixote", "Miguel de Cervantes", "Don Quixote by Miguel de Cervantes", "Parody", 1250, 45);
		Book b3 = new Book("One Hundred Years of Solitude", "Gabriel Garcia Marquez",
				"One Hundred Years of Solitude by Gabriel Garcia Marquez", "Autobiography", 980, 43);
		Book b4 = new Book("Hamlet", "William Shakespeare", "Hamlet by William Shakespeare", "Drama", 500, 65);
		Book b5 = new Book("In Search of Lost Time", "Marcel Prous", "In Search of Lost Time by Marcel Prous", "Autobiography", 1270, 12);
		
		
		User u1 = new User("akash", "Aakash Verma", "2356898727", "akash@gmail.com", "akash123");
		User u2 = new User("gaurav", "Gaurav Singh", "9576898762", "gaurav@gmail.com", "gaurav123");
		User u3 = new User("arjun", "Arjun Singh", "1786898749", "arjun@gmail.com", "arjun123");
		User u4 = new User("arvind", "Arvind Yadav", "8476898775", "arvind@gmail.com", "arvind123");
		
		
		
		// add book to users
		//u1.getBooks_wishlist().add(bookRepository.findByBook_id(3));
		//u1.getBooks_wishlist().add(b2);
		//u2.getBooks_wishlist().add(b4);
		
		// add user to books
		
		//b1.getUsers_wishlist().add(u4);
		
	
//		//System.out.println(this.bookRepository.findByBookId(9));
		this.userRepository.save(u1);
		this.userRepository.save(u2);
		this.userRepository.save(u3);
		this.userRepository.save(u4);
		this.bookRepository.save(b1);
		this.bookRepository.save(b2);
		this.bookRepository.save(b3);
		this.bookRepository.save(b4);
		this.bookRepository.save(b5);
//		
		
//		User user = this.userRepository.findByUserId(3);
//		Book book1 = this.bookRepository.findByBookId(1);
//		
//		List<Completedlist> com= this.userService.removeFromCompletedlist(user, book1);
//		
//		
//	
//		System.out.println(com.size());
//		
	}
	
	
}
