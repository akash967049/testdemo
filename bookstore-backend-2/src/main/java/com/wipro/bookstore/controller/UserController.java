package com.wipro.bookstore.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wipro.bookstore.entity.Book;
import com.wipro.bookstore.entity.Completedlist;
import com.wipro.bookstore.entity.User;
import com.wipro.bookstore.entity.UserBookdata;
import com.wipro.bookstore.entity.Wishlist;
import com.wipro.bookstore.service.UserService;


@CrossOrigin
@RestController
public class UserController {
	
	@Autowired
	private UserService userService;
	
	
	@PostMapping("/adduser")
	public User addUser(@RequestBody User user) {
		return userService.saveUser(user);
	}
	
	@PostMapping("/addusers")
	public List<User> addUsers(@RequestBody List<User> users) {
		return userService.saveUser(users);
	}
	
	@GetMapping("/users")
	public List<User> findAllUsers(){
		return userService.getUsers();
	}
	
	@GetMapping("/userbyid/{id}")
	public User findUserById(@PathVariable int id) {
		return userService.getUserById(id);
	}
	
	@PostMapping("/userbyname")
	public User findUserByName(@RequestBody String username) {
		return userService.getUserByUsername(username);
	}
	
	@DeleteMapping("/delete/{id}")
	public String deleteUser(@PathVariable int id) {
		return userService.deleteUser(id);
	}
	
	@PostMapping("/loginuser")
	public User loginUserControl(@RequestBody User data) {
		return userService.loginUser(data);
	}
	
	@PostMapping("/registeruser")
	public User registerUserControl(@RequestBody User data) {
		return userService.registerUser(data);
	}
	
	@GetMapping("/sayhi")
	public String sayHi() {
		System.out.println("you were here");
		return "You are most Welcome";
	}
	
	// Book specific requests
	
	
	@PostMapping("/addbook")
	public Book addBook(@RequestBody Book book) {
		return userService.saveBook(book);
	}
	
	@PostMapping("/addbooks")
	public List<Book> addBooks(@RequestBody List<Book> books) {
		return userService.saveBooks(books);
	}
	
	@GetMapping("/books")
	public List<Book> findAllBooks(){
		return userService.getBooks();
	}
	
	@GetMapping("/bookbyid/{id}")
	public Book findBookById(@PathVariable int id) {
		return userService.getBookById(id);
	}
	
	@PostMapping("/registerbook")
	public Book registerBookControl(@RequestBody Book data) {
		return userService.registerBook(data);
	}
	
	// add to wishlist api
	
	@PostMapping("/addtowishlist")
	public List<Book> addbooktowishlist(@RequestBody UserBookdata data) {
		User userdata = data.getUser();
		Book book = data.getBook();
		return userService.addToWishlist(userdata, book);
	}
	
	// remove from wishlist
	
	@PostMapping("/removefromwishlist")
	public List<Book> removebooktowishlist(@RequestBody UserBookdata data) {
		User userdata = data.getUser();
		Book book = data.getBook();
		return userService.removeFromWishlist(userdata, book);
	}
	
	// add to completd api
	
		@PostMapping("/addtocompletedlist")
		public List<Book> addbooktocompletedlist(@RequestBody UserBookdata data) {
			User userdata = data.getUser();
			Book book = data.getBook();
			return userService.addToCompletedlist(userdata, book);
		}
		
		// remove from completed
		
		@PostMapping("/removefromcompletedlist")
		public List<Book> removebooktocompletedlist(@RequestBody UserBookdata data) {
			User userdata = data.getUser();
			Book book = data.getBook();
			return userService.removeFromCompletedlist(userdata, book);
		}
		
		// get completedlist
		
		@PostMapping("/completedlist")
		public List<Book> getcompletedlist(@RequestBody User data) {
			
			return userService.getCompletedBooks(data);
		}
		
		// get wishlist
		
		@PostMapping("/wishlist")
		public List<Book> getwishlist(@RequestBody User data) {
			
			return userService.getWishlistBooks(data);
		}
		
		// check for wishlist
		
		@PostMapping("/checkfromcompletedlist")
		public String checkbooktocompletedlist(@RequestBody UserBookdata data) {
			User userdata = data.getUser();
			Book book = data.getBook();
			return userService.checkForCompletedlist(userdata, book);
		}
		
		@PostMapping("/checkfromwishlist")
		public String checkbooktowishlist(@RequestBody UserBookdata data) {
			User userdata = data.getUser();
			Book book = data.getBook();
			return userService.checkForWishlist(userdata, book);
		}

}
