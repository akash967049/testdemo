package com.wipro.bookstore.entity;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;





@Entity
@Table(name= "books")
public class Book {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int bookId;
	
	private String bookName;
	private String genre;
	private String Author;
	private String Description;
	private int bookPrice;
	private int bookSold;

	
	public Book() {
		// TODO Auto-generated constructor stub
	}

	public Book(String bookName, String author, String description, String genre, int bookPrice, int bookSold) {
		super();
		this.bookName = bookName;
		this.genre = genre;
		Author = author;
		Description = description;
		this.bookPrice = bookPrice;
		this.bookSold = bookSold;
	}

	
	

	public Book(int bookId, String bookName, String genre, String author, String description, int bookPrice,
			int bookSold) {
		super();
		this.bookId = bookId;
		this.bookName = bookName;
		this.genre = genre;
		Author = author;
		Description = description;
		this.bookPrice = bookPrice;
		this.bookSold = bookSold;
	}

	public int getBookId() {
		return bookId;
	}

	public void setBookId(int bookId) {
		this.bookId = bookId;
	}

	
	public String getBookName() {
		return bookName;
	}

	public void setBookName(String bookName) {
		this.bookName = bookName;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public String getAuthor() {
		return Author;
	}

	public void setAuthor(String author) {
		Author = author;
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		Description = description;
	}

	public int getBookPrice() {
		return bookPrice;
	}

	public void setBookPrice(int bookPrice) {
		this.bookPrice = bookPrice;
	}

	public int getBookSold() {
		return bookSold;
	}

	public void setBookSold(int bookSold) {
		this.bookSold = bookSold;
	}

}
