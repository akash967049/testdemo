package com.wipro.bookstore.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "completedlist")
public class Completedlist {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int completedId;
	
	private int userId;
	private int bookId;
	
	
	
	public Completedlist() {
		super();
	}


	public Completedlist(int userId, int bookId) {
		super();
		this.userId = userId;
		this.bookId = bookId;
	}
	
	
	public int getCompletedId() {
		return completedId;
	}
	public void setCompletedId(int completedId) {
		this.completedId = completedId;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getBookId() {
		return bookId;
	}
	public void setBookId(int bookId) {
		this.bookId = bookId;
	}
	
	

}
