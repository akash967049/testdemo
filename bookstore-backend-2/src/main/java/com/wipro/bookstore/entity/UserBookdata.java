package com.wipro.bookstore.entity;

public class UserBookdata {
	
	private User user;
	private Book book;
	
	
	public UserBookdata(User user, Book book) {
		super();
		this.user = user;
		this.book = book;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Book getBook() {
		return book;
	}
	public void setBook(Book book) {
		this.book = book;
	}
	
	

}
