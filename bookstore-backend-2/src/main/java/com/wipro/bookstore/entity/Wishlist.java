package com.wipro.bookstore.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "wishlist")
public class Wishlist {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int wishId;
	
	private int userId;
	private int bookId;
	
	
	public Wishlist() {
		super();
	}

	public Wishlist(int userId, int bookId) {
		super();
		this.userId = userId;
		this.bookId = bookId;
	}
	
	
	public int getWishId() {
		return wishId;
	}
	
	public void setWishId(int wishId) {
		this.wishId = wishId;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getBookId() {
		return bookId;
	}
	public void setBookId(int bookId) {
		this.bookId = bookId;
	}
	
	
}
