package com.wipro.bookstore.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.wipro.bookstore.entity.Book;

public interface BookRepository extends JpaRepository<Book, Integer> {

	Book findByBookId(int id);
	
	

}
