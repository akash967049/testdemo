package com.wipro.bookstore.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.wipro.bookstore.entity.Completedlist;

public interface CompletedlistRepository extends JpaRepository<Completedlist, Integer>{

	List<Completedlist> findByUserId(int id);
	
	List<Completedlist> findByCompletedId(int id);
	
}
