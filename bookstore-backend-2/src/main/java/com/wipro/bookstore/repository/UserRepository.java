package com.wipro.bookstore.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.wipro.bookstore.entity.User;

public interface UserRepository extends JpaRepository<User, Integer> {


	User findByUsername(String name);
	
	User findByUserId(int id);

}
