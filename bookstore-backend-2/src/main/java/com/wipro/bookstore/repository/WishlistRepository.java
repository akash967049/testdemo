package com.wipro.bookstore.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.wipro.bookstore.entity.Wishlist;

public interface WishlistRepository extends JpaRepository<Wishlist, Integer> {
	
	List<Wishlist> findByUserId(int id);

}
