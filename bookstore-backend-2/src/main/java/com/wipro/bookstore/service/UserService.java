package com.wipro.bookstore.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wipro.bookstore.entity.Book;
import com.wipro.bookstore.entity.Completedlist;
import com.wipro.bookstore.entity.User;
import com.wipro.bookstore.entity.Wishlist;
import com.wipro.bookstore.repository.BookRepository;
import com.wipro.bookstore.repository.CompletedlistRepository;
import com.wipro.bookstore.repository.UserRepository;
import com.wipro.bookstore.repository.WishlistRepository;

@Service
public class UserService {

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private BookRepository bookRepository;
	
	@Autowired
	private WishlistRepository wishRepository;
	
	@Autowired
	private CompletedlistRepository completedRepository;
	
	
	public User saveUser(User user) {
		return userRepository.save(user);
	}
	
	public List<User> saveUser(List<User> users) {
		return userRepository.saveAll(users);
	}
	
	public List<User> getUsers(){
		return userRepository.findAll();
	}
	
	public User getUserById(int id) {
		return userRepository.findByUserId(id);
	}
	
	public User getUserByUsername(String name) {
		return userRepository.findByUsername(name);
	}
	
	public String deleteUser(int id) {
		userRepository.deleteById(id);
		return "user removed || "+id;
	}
	
	public User loginUser(User data) {
		List<User> users = getUsers();
		User per = null;
		for(User user: users) {
			if(user.getUsername().equals(data.getUsername()) && user.getPassword().equals(data.getPassword())) {
				per = user;
				break;
			}
		}
		return per;
	}
	
	public User registerUser(User data) {
		List<User> users = getUsers();
		User per = new User();
		for(User user: users) {
			if(user.getUsername().equals(data.getUsername())) {
				per = user;
				break;
			}
		}
		if(per.getUsername()==null) {
			return saveUser(data);
		}else {
			return null;
		}
	}
	
	// book adding and deleting in database
	
	public Book saveBook(Book book) {
		return bookRepository.save(book);
	}
	
	public List<Book> saveBooks(List<Book> books) {
		return bookRepository.saveAll(books);
	}
	
	public List<Book> getBooks(){
		return bookRepository.findAll();
	}
	
	public Book getBookById(int id) {
		return bookRepository.findByBookId(id);
	}
	
	// adding or deleting book
	
	public Book registerBook(Book data) {
		List<Book> books = getBooks();
		Book per = new Book();
		for(Book book: books) {
			if(book.getBookName().equals(data.getBookName())) {
				per = book;
				break;
			}
		}
		if(per.getBookName()==null) {
			return saveBook(data);
		}else {
			return null;
		}
	}
	
	
	// add or remove from wishlist
	
	public List<Book> addToWishlist(User user,Book book){
		this.wishRepository.save(new Wishlist(user.getUserId(), book.getBookId()));
		List<Book> books = this.getallWbookbyuser(user, this.wishRepository.findByUserId(user.getUserId()));
		return books;
	}
	
	public List<Book> removeFromWishlist(User user,Book book){
		List<Wishlist> wishes = this.wishRepository.findAll();
		Wishlist userwish = new Wishlist(user.getUserId(), book.getBookId());
		for(Wishlist wish: wishes) {
			if(wish.getBookId()==userwish.getBookId() && wish.getUserId()==userwish.getUserId()) {
				userwish.setWishId(wish.getWishId());
				break;
			}
		}
		this.wishRepository.deleteById(userwish.getWishId());
		List<Book> books = this.getallWbookbyuser(user, this.wishRepository.findByUserId(user.getUserId()));
		return books;
	}
	
	// add or remove from completed
	
	public List<Book> addToCompletedlist(User user,Book book){
		this.completedRepository.save(new Completedlist(user.getUserId(), book.getBookId()));
		List<Book> books = this.getallCbookbyuser(user, this.completedRepository.findByUserId(user.getUserId()));
		return books;
	}
	
	public List<Book> removeFromCompletedlist(User user,Book book){
		List<Completedlist> completes = this.completedRepository.findAll();
		Completedlist userwish = new Completedlist(user.getUserId(), book.getBookId());
		for(Completedlist complete: completes) {
			if(complete.getBookId()==userwish.getBookId() && complete.getUserId()==userwish.getUserId()) {
				userwish.setCompletedId(complete.getCompletedId());
				System.out.println("i was also here");
				break;
			}
		}
		System.out.println(userwish.getCompletedId());
		this.completedRepository.deleteById(userwish.getCompletedId());
		List<Book> books = this.getallCbookbyuser(user, this.completedRepository.findByUserId(user.getUserId()));
		return books;
	}
	
	public List<Book>  getallCbookbyuser(User user, List<Completedlist> completedlist){
		List<Book> books = new ArrayList<>();
		for(Completedlist item: completedlist) {
			Book book = this.bookRepository.findByBookId(item.getBookId());
			books.add(book);
		}
		return books;
	}
	
	public List<Book>  getallWbookbyuser(User user, List<Wishlist> wishlist){
		List<Book> books = new ArrayList<>();
		for(Wishlist item: wishlist) {
			Book book = this.bookRepository.findByBookId(item.getBookId());
			books.add(book);
		}
		return books;
	}
	

	public List<Book> getWishlistBooks(User data){
		User user = this.loginUser(data);
		if(user.getName()!=null) {
			List<Wishlist> wishes = this.wishRepository.findByUserId(user.getUserId());
			return this.getallWbookbyuser(user, wishes);
		}else {
			return null;
		}
	}
	
	public List<Book> getCompletedBooks(User data){
		User user = new User();
		user = this.loginUser(data);
		System.out.println(user.getUserId());
		if(user.getName()!=null) {
			List<Completedlist> com = this.completedRepository.findByUserId(user.getUserId());
			return this.getallCbookbyuser(user, com);
		}else {
			return null;
		}
	}
	
	// check for userwishlist
	
	public String checkForWishlist(User user, Book book) {
		List<Book> books= this.getWishlistBooks(user);
		for(Book item : books) {
			if(book.getBookId()==item.getBookId()) {
				return "yes";
			}
		}
		return "no";
	}
	
	// check for Completed
	
	public String checkForCompletedlist(User user, Book book) {
		List<Book> books= this.getCompletedBooks(user);
		for(Book item : books) {
			if(book.getBookId()==item.getBookId()) {
				return "yes";
			}
		}
		return "no";
	}
	
	
	
}
